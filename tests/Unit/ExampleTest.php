<?php
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;


class ExampleTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic database test example.
     *
     * @return void
     */
    public function testDatabase()
    {
        $user = factory(App\User::class)->create();
        $this->seeInDatabase('users', [
            'email' => $user->email
        ]);
    }
}